package pl.oscdev.gliderflightlog;

import java.util.Calendar;
import java.util.Locale;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ScrollView;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

public class dodajlot extends Activity implements OnClickListener {

  Button zapisz;
  int pickerID;
  final Calendar c = Calendar.getInstance(Locale.getDefault());
  EditText czasStartu, czasLadowania, data;
  TimePickerDialog czasPickerDialog;
  DatePickerDialog dataPickerDialog;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.dodajlot);

    ScrollView dodajLot = (ScrollView)findViewById(R.id.dodajLot);
    dodajLot.smoothScrollTo(0,0);

    czasStartu = (EditText)findViewById(R.id.czasStartu);
    czasStartu.setOnClickListener(this);

    czasLadowania = (EditText)findViewById(R.id.czasLadowania);
    czasLadowania.setOnClickListener(this);

    data = (EditText)findViewById(R.id.data);
    data.setOnClickListener(this);

    zapisz = (Button)findViewById(R.id.zapisz);
    zapisz.setOnClickListener(this);

  }


  @Override
  public Dialog onCreateDialog(int id) {
    //Log.d("aa", Integer.toString(czasPicker));
    switch ( id ) {

      case 0:
        dataPickerDialog = new DatePickerDialog(this, dataSetListener, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH) );
        return dataPickerDialog;

      case 1:
        czasPickerDialog = new TimePickerDialog(this, czasSetListener, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), true);
        return czasPickerDialog;

    }

    return null;

  }


  private TimePickerDialog.OnTimeSetListener czasSetListener =
    new TimePickerDialog.OnTimeSetListener() {
      @Override
      public void onTimeSet(TimePicker view, int hour, int minute) {
         ((EditText) findViewById( pickerID )).setText(Integer.toString(hour) + ':' + String.format("%02d", minute));
      }
    };

  private DatePickerDialog.OnDateSetListener dataSetListener =
      new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
          monthOfYear++;
          ((EditText) findViewById( pickerID )).setText( Integer.toString(dayOfMonth) + '.' + String.format("%02d", monthOfYear) + '.' + Integer.toString(year));
        }
      };

  @Override
  public void onClick(View v) {

    switch (v.getId()) {

      case R.id.zapisz:

        InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

        Toast toast = Toast.makeText(getApplicationContext(), R.string.zapisano, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.show();

        break;

      case R.id.data:

        pickerID = v.getId();
        showDialog(0);

        break;

      case R.id.czasStartu:

        pickerID = v.getId();
        showDialog(1);

        break;

      case R.id.czasLadowania:

        pickerID = v.getId();
        showDialog(1);

        break;

    }

  }

}