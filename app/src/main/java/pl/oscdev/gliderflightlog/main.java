package pl.oscdev.gliderflightlog;


import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Menu;
import android.view.MenuItem;


public class main extends Activity implements OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

      super.onCreate(savedInstanceState);
      setContentView(R.layout.main);

      View dodajLot = findViewById(R.id.dodajLot);
      dodajLot.setOnClickListener(this);

      View konfiguracja = findViewById(R.id.konfiguracja);
      konfiguracja.setOnClickListener(this);

      View wyjscie = findViewById(R.id.wyjscie);
      wyjscie.setOnClickListener(this);

    }

  @Override
  public void onClick(View v) {

    switch (v.getId()) {

      case R.id.konfiguracja:
        startActivity(new Intent(this, konfiguracja.class));
        break;

      case R.id.dodajLot:
        startActivity(new Intent(this, dodajlot.class));
        break;

      case R.id.listaLotow:
        startActivity(new Intent(this, listalotow.class));
        break;

      case R.id.statystyki:
        startActivity(new Intent(this, statystyki.class));
        break;

      case R.id.wyjscie:
        finish();
        break;

    }

  }


  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
      getMenuInflater().inflate(R.menu.main, menu);
      return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {

    switch (item.getItemId()) {
      case R.id.konfiguracja:
        startActivity(new Intent(this,konfiguracja.class));
        break;
      case R.id.autor:
        startActivity(new Intent(this,autor.class));
        break;
    }

    return true;
  }


}
