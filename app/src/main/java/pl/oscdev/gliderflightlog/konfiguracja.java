package pl.oscdev.gliderflightlog;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Button;
import android.widget.Toast;
import android.view.Menu;
import android.view.MenuItem;


public class konfiguracja extends Activity implements OnClickListener {

  Button zapisz;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.konfiguracja);

    getPreferences("szybowiecJednomiejscowy");

    getPreferences("szybowiecDwumiejscowy");

    getPreferences("kosztStartSamolotMin");

    getPreferences("kosztStartSamolotMetry");

    getPreferences("kosztStartWyciagarka");

    zapisz = (Button)findViewById(R.id.zapisz);
    zapisz.setOnClickListener(this);

  }

  @Override
  public void onClick(View v) {

    switch (v.getId()) {

      case R.id.zapisz:

        InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

        savePreferences("szybowiecJednomiejscowy");

        savePreferences("szybowiecDwumiejscowy");

        savePreferences("kosztStartSamolotMin");

        savePreferences("kosztStartSamolotMetry");

        savePreferences("kosztStartWyciagarka");

        Toast toast = Toast.makeText(getApplicationContext(), R.string.zapisano, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.show();

        break;

    }

  }

  private void savePreferences ( String view ) {
    int viewID = getApplicationContext().getResources().getIdentifier(view, "id", getPackageName());
    EditText editText = (EditText)findViewById(viewID);
    getSharedPreferences(view, 0).edit().putString(view, editText.getText().toString()).commit();
  }

  private void getPreferences ( String view ) {
    int viewID = getApplicationContext().getResources().getIdentifier(view, "id", getPackageName());
    EditText editText = (EditText)findViewById(viewID);
    editText.setText(getSharedPreferences(view, 0).getString(view, "0"));
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {

    switch (item.getItemId()) {
      case R.id.konfiguracja:
        startActivity(new Intent(this,konfiguracja.class));
        break;
      case R.id.autor:
        startActivity(new Intent(this,autor.class));
        break;
    }

    return true;
  }

}
